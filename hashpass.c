#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
    //printf("%s\n%s\n", argv[1], argv[2]);
    
    FILE *f,*read;
    read=fopen(argv[1], "r" );
    if(!read)
    {
        printf("Can't open %s\n\n", argv[1]);
    }
    
    f=fopen(argv[2], "a");
    if(!f)
    {
        printf("Can't open %s\n\n", argv[2]);
    }
    
    char line[100];

    while(fgets(line, 100, read) != NULL)
    {
        char *hash = md5(line, strlen(line)-1);
        fprintf(f, "%s\n", hash);
        //printf("%lu\n",strlen(line));
        free(hash);
    }
    
    
    fclose(f);
    fclose(read);
    
    
}